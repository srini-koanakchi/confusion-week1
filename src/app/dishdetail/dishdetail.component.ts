import { Component, OnInit, Input , Inject} from '@angular/core';
import { Dish } from '../shared/dish';
import { Comment} from '../shared/comment';

import {Params, ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';
import { DishService } from '../services/dish.service';

import 'rxjs/add/operator/switchMap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { visibility, flyInOut , expand} from '../animations/app.animation';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
    animations: [
      visibility(),
      flyInOut(),
      expand()
    ]  
})
export class DishdetailComponent implements OnInit {

  dish: Dish;
  dishCopy = null;
  dishIds: number[];
  prev: number;
  next: number;

  commentsForm: FormGroup;
  commentVO: Comment;
  errMess: string;
  visibility = 'shown';

  formErrors = {
    'comment': '',
    'author': ''    
  };

  validationMessages = {
    'comment': {
      'required':      'Comment is required.'      
    },
    'author': {
      'required':      'Name is required.',
      'minlength':     'Name must be at least 2 characters long.',
      'maxlength':     'Name cannot be more than 25 characters long.'
    }
  };
 
  constructor(private dishservice: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') private BaseURL) {
      this.createForm();
     }

  ngOnInit() {

      this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds, errmess => this.errMess=  <any>errmess);
      this.route.params
        .switchMap((params: Params) => {  this.visibility = 'hidden'; return this.dishservice.getDish(+params['id'] ); } )
        .subscribe(dish => { this.dish = dish; this.dishCopy = dish; this.setPrevNext(dish.id);  this.visibility = 'shown'; });
    }
  
    setPrevNext(dishId: number) {
      let index = this.dishIds.indexOf(dishId);
      this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
      this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
    }

  goBack(): void {
    this.location.back();
  }

  createForm() {
    this.commentsForm = this.fb.group({
      comment: ['', [Validators.required] ],
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],     
      rating: 5, 
    });

    this.commentsForm.valueChanges
    .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now

  }

  onSubmit() {
    this.commentVO = this.commentsForm.value;
    this.commentVO.date = new Date().toISOString();;
    console.log(this.commentVO);

    this.dishCopy.comments.push(this.commentVO);
    this.dishCopy.save()
                .subscribe(dish => this.dish = dish);
    this.commentsForm.reset({
      comment: '',
      author: '',
      rating: '5',
    });
  }

  onValueChanged(data?: any) {
    if (!this.commentsForm) { return; }
    const form = this.commentsForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

}
